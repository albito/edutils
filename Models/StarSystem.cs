﻿
using System.ComponentModel;

namespace EDUtils.Models
{
    public class StarSystem : Coordinates
    {
        public const string TYPE_NEUTRON = "non_sequence";
        public string name;
        public string type;

        public string Name {
            get { return this.name; }
            set {
                this.name = value;
                this.OnPropertyChanged("Name");
            }
        }
        public string Type {
            get { return this.type; }
            set {
                this.type = value;
                this.OnPropertyChanged("Type");
            }
        }
        
        public StarSystem(string name="", string type="", float x=0.0f, float y=0.0f, float z=0.0f) : base(x, y, z)
        {
            this.name = name;
            this.type = type;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if(PropertyChanged!=null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
