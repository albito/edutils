﻿
namespace EDUtils.Models
{
    class StarShip
    {
        public string name;
        public float jumpRange;
        public float fuelTons;

        public StarShip(string name="", float jumpRange=0.0f, float fuelTons=0.0f)
        {
            this.name = name;
            this.jumpRange = jumpRange;
            this.fuelTons = fuelTons;
        }
    }
}
