﻿using System.Windows.Input;

namespace EDUtils.Models
{
    public class KeyCombination
    {
        public Key key = Key.None;
        public ModifierKeys modifier = ModifierKeys.None;

        public uint VKKey {
            get {
                return (uint)KeyInterop.VirtualKeyFromKey(this.key);
            }
        }
        public uint VKModifier {
            get {
                return (uint)this.modifier;
            }
        }

        public KeyCombination(Key key=Key.None, ModifierKeys modifier=ModifierKeys.None)
        {
            this.SetValues(key, modifier);
        }

        public KeyCombination(ushort key, ushort modifier=0)
        {
            this.SetValues(key, modifier);
        }

        public void SetValues(Key key=Key.None, ModifierKeys modifier=ModifierKeys.None)
        {
            this.key = key;
            this.modifier = modifier;
        }

        public void SetValues(ushort key, ushort modifier=0)
        {
            this.key = (Key)key;
            this.modifier = (ModifierKeys)modifier;
        }

        public static ModifierKeys ModifierFromKey(Key key)
        {
            ModifierKeys modKey = ModifierKeys.None;
            switch(key)
            {
                case Key.LeftCtrl:
                case Key.RightCtrl:
                    modKey = ModifierKeys.Control;
                    break;
                case Key.LeftShift:
                case Key.RightShift:
                    modKey = ModifierKeys.Shift;
                    break;
                case Key.LeftAlt:
                case Key.RightAlt:
                    modKey = ModifierKeys.Alt;
                    break;
                case Key.LWin:
                case Key.RWin:
                    modKey = ModifierKeys.Windows;
                    break;
                default:
                    break;
            }
            return modKey;
        }

        public bool HasKey()
        {
            return this.key!=Key.None;
        }

        public bool HasModifier()
        {
            return this.modifier!=ModifierKeys.None;
        }

        public bool IsEmpty()
        {
            string keyString = "";
            if (modifier != ModifierKeys.None)
            {
                keyString = modifier.ToString() + " + ";
            }
            if (key != Key.None)
            {
                keyString += key.ToString();
            }
            return this.key==Key.None && this.modifier==ModifierKeys.None;
        }

        public override string ToString()
        {
            string keyString = "";
            if(modifier!=ModifierKeys.None)
            {
                keyString = modifier.ToString()+" + ";
            }
            if(key!=Key.None)
            {
                keyString += key.ToString();
            }
            return keyString;
        }
    }
}
