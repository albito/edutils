﻿using Newtonsoft.Json;
using System;

namespace EDUtils.Models.EDEvents
{
    /// <summary>
    /// Game event base model.
    /// </summary>
    public class EDEvent
    {
        /// <summary>
        /// Event timestamp.
        /// </summary>
        public DateTime timestamp;

        /// <summary>
        /// Event name.
        /// </summary>
        [JsonProperty("event")]
        public string eventName;

    }
}
