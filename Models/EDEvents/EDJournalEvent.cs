﻿using System;
using System.Collections.Generic;

namespace EDUtils.Models.EDEvents
{
    /// <summary>
    /// Game journal event.
    /// </summary>
    public class EDJournalEvent : EDEvent
    {
        /// <summary>
        /// Journal event names
        /// </summary>
        public static class JOURNAL_EVENT {
                public const string FILE_HEADER = "FileHeader";
                public const string CONTINUE = "Continue";
                public const string LOCATION = "Location";
                public const string FSD_TARGET = "FSDTarget";
                public const string START_JUMP = "StartJump";
                public const string FSD_JUMP = "FSDJump";
                public const string JET_CONE_BOOST = "JetConeBoost";
            };

        /// <summary>
        /// Journal event name comparer.
        /// </summary>
        public class EDJournalEventNameComparer : IEqualityComparer<EDJournalEvent> {
                public bool Equals(EDJournalEvent x, EDJournalEvent y) {
                        return x.eventName.Equals(y.eventName, StringComparison.OrdinalIgnoreCase);
                    }
                public int GetHashCode(EDJournalEvent obj) {
                        return obj.GetHashCode();
                    }
            };
        
        public string From;
        public string Message;
        public string Message_Localised;

        public string Name;

        public string SystemAddress;

        public string JumpType;

        public string StarSystem;

        public string StarClass;

        public double[] StarPos;

        public float JumpDist;
        public float FuelUsed;
        public float FuelLevel;
        public float BoostUsed;
        public float BoostValue;

        public string MusicTrack;
    }
}
