﻿
namespace EDUtils.Models.EDEvents
{
    /// <summary>
    /// Game status event.
    /// </summary>
    public class EDStatusEvent : EDEvent
    {
        /// <summary>
        /// Journal events
        /// </summary>
        public enum STATUS_EVENT {
                FLAGS,
                GUI_FOCUS,
                FUEL,
                CARGO,
                PIPS,
                FIRE_GROUP,
                LOCATION
            };
        /// <summary>
        /// Gui focus values.
        /// </summary>
        public enum GUI_FOCUS : int {
                NONE = 0,
                PANEL_INTERNAL = 1,
                PANEL_EXTERNAL = 2,
                PANEL_COMMUNICATIONS = 3,
                PANEL_ROLE = 4,
                STATION_SERVICES = 5,
                GALAXY_MAP = 6,
                SYSTEM_MAP = 7,
                ORRERY = 8,
                FSS_MODE = 9,
                SAA_MODE = 10,
                CODEX = 11
        };
        /// <summary>
        /// Legal state values.
        /// </summary>
        public static class LEGAL_STATE {
                public const string CLEAN = "Clean";
                public const string ILLEGAL_CARGO = "IllegalCargo";
                public const string SPEEDING = "Speeding";
                public const string HOSTILE = "Hostile";
                public const string WANTED = "Wanted";
                public const string PASSENGER_WANTED = "PassengerWanted";
                public const string WARRANT = "Warrant";
            };

        /// <summary>
        /// Status flags encoded as bits in an integer.
        /// </summary>
        public int Flags;
        /// <summary>
        /// The selected GUI screen.
        /// </summary>
        public int GuiFocus;
        /// <summary>
        /// Fuel mass information.
        /// </summary>
        public StatusEventFuel Fuel = new StatusEventFuel();
        /// <summary>
        /// Cargo mass in tons.
        /// </summary>
        public float Cargo;
        /// <summary>
        /// Array of 3 integers representing energy distribution (in half-pips).
        /// </summary>
        public int[] Pips;
        /// <summary>
        /// The currently selected firegroup number.
        /// </summary>
        public int FireGroup;
        /// <summary>
        /// Current legal state.
        /// </summary>
        public string LegalState;
        /// <summary>
        /// Latitude (if on or near a planet).
        /// </summary>
        public float Latitude;
        /// <summary>
        /// Longitude (if on or near a planet).
        /// </summary>
        public float Longitude;
        /// <summary>
        /// Altitude (if on or near a planet).
        /// </summary>
        public int Altitude;
        /// <summary>
        /// Heading (if on or near a planet).
        /// </summary>
        public int Heading;
        /// <summary>
        /// Planet name (if on or near a planet).
        /// </summary>
        public string BodyName;

        /// <summary>
        /// Fuel mass.
        /// </summary>
        public class StatusEventFuel
        {
            /// <summary>
            /// Fuel mass in tons.
            /// </summary>
            public float FuelMain;
            /// <summary>
            /// Fuel reservoir in tons.
            /// </summary>
            public float FuelReservoir;

            /// <summary>
            /// Checks values equality with another StatusEventFuel object.
            /// </summary>
            /// <param name="obj">The object to check with.</param>
            /// <returns>True if all values matches.</returns>
            public bool Equals(StatusEventFuel obj)
            {
                return obj != null
                    && this.FuelMain == obj.FuelMain
                    && this.FuelReservoir == obj.FuelReservoir;
            }

            /// <summary>
            /// Checks values equality with another object.
            /// </summary>
            /// <param name="obj">The object to check with.</param>
            /// <returns>True if object is StatusEventFuel and all values matches.</returns>
            public override bool Equals(object obj)
            {
                return obj != null
                    && obj is StatusEventFuel
                    && this.Equals((StatusEventFuel)obj);
            }
        }

        /// <summary>
        /// Checks values equality with another EDStatusEvent object.
        /// </summary>
        /// <param name="obj">The object to check with.</param>
        /// <returns>True if all values matches.</returns>
        public bool Equals(EDStatusEvent obj)
        {
            return obj != null
                && this.eventName == obj.eventName
                //&& this.timestamp == obj.timestamp
                && this.Flags == obj.Flags
                && this.GuiFocus == obj.GuiFocus
                && this.Fuel.Equals(obj.Fuel);
                //&& this.Cargo == obj.Cargo
                //&& Enumerable.SequenceEqual(this.Pips, obj.Pips)
                //&& this.FireGroup == obj.FireGroup
                //&& this.Latitude == obj.Latitude
                //&& this.Longitude == obj.Longitude
                //&& this.Altitude == obj.Altitude
                //&& this.Heading == obj.Heading;
        }

        /// <summary>
        /// Checks values equality with another object.
        /// </summary>
        /// <param name="obj">The object to check with.</param>
        /// <returns>True if object is EDStatusEvent and all values matches.</returns>
        public override bool Equals(object obj)
        {
            return obj != null
                && obj is EDStatusEvent
                && this.Equals((EDStatusEvent)obj);
        }
    }
}
