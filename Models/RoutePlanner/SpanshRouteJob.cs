﻿
namespace EDUtils.Models.SpanshRoute
{
    public class SpanshRouteJob
    {
        public const string STATUS_OK = "ok";
        public const string STATUS_QUEUED = "queued";

        public string job = "";
        public string status = "";
        public SpanshRouteResult result = null;
        
    }
}
