﻿using System.Collections.Generic;

namespace EDUtils.Models.SpanshRoute
{
    public class SpanshRouteResult
    {
        public string job = "";
        public string source_system = "";
        public string destination_system = "";
        public List<string> via = new List<string>();
        public string range = "";
        public string efficiency = "";
        public double distance = 0.0;
        public int total_jumps = 0;
        public List<SpanshRouteStarSystem> system_jumps = new List<SpanshRouteStarSystem>();
    }
}
