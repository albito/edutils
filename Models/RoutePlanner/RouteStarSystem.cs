﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace EDUtils.Models
{
    public abstract class RouteStarSystem : StarSystem
    {
        public abstract double Distance { get; set; }

        public int jumps = 0;
        protected double jumpedDistance = 0.0;
        protected double remainingDistance = 0.0;
        protected int jumpsCount = 0;
        public int routeStep = 0;
        public double routeStepPercent = 0.0;
        public double jumpsCountPercent = 0.0;
        public double jumpedDistancePercent = 0.0;

        public string system {
            get { return this.name; }
            set {
                this.Name = value;
                this.OnPropertyChanged("system");
            }
        }
        public bool IsNeutronStar {
            get { return TYPE_NEUTRON.Equals(this.type, System.StringComparison.OrdinalIgnoreCase); }
            set {
                this.type = value? TYPE_NEUTRON : "";
                this.OnPropertyChanged("IsNeutronStar");
            }
        }
        public int Jumps {
            get { return this.jumps; }
            set {
                this.jumps = value;
                this.OnPropertyChanged("Jumps");
            }
        }
        public double JumpedDistance {
            get { return this.jumpedDistance; }
            set {
                this.jumpedDistance = value;
                this.OnPropertyChanged("JumpedDistance");
            }
        }
        public double RemainingDistance {
            get { return this.remainingDistance; }
            set {
                this.remainingDistance = value;
                this.OnPropertyChanged("RemainingDistance");
            }
        }
        public int JumpsCount {
            get { return this.jumpsCount; }
            set {
                this.jumpsCount = value;
                this.OnPropertyChanged("JumpsCount");
            }
        }
        public int RouteStep {
            get { return this.routeStep; }
            set {
                this.routeStep = value;
                this.OnPropertyChanged("RouteStep");
            }
        }
        public double RouteStepPercent {
            get { return this.routeStepPercent; }
            set {
                this.routeStepPercent = value;
                this.OnPropertyChanged("RouteStepPercent");
            }
        }
        public double JumpsCountPercent {
            get { return this.jumpsCountPercent; }
            set {
                this.jumpsCountPercent = value;
                this.OnPropertyChanged("JumpsCountPercent");
            }
        }
        public double JumpedDistancePercent {
            get { return this.jumpedDistancePercent; }
            set {
                this.jumpedDistancePercent = value;
                this.OnPropertyChanged("JumpedDistancePercent");
            }
        }

        public static void CalculateRouteData(ICollection<RouteStarSystem> items)
        {
            if(items.Count()>0)
            {
                items.FirstOrDefault().jumps = 0;
            }
            RouteStarSystem lastItem = null;
            foreach(RouteStarSystem item in items)
            {
                if(lastItem!=null)
                {
                    item.JumpsCount = item.Jumps + lastItem.JumpsCount;
                    item.JumpedDistance = item.Distance + lastItem.JumpedDistance;
                    item.RouteStep = lastItem.RouteStep + 1;
                }
                lastItem = item;
            }
            foreach(RouteStarSystem item in items)
            {
                item.RemainingDistance = lastItem.JumpedDistance - item.JumpedDistance;
                item.RouteStepPercent = (double) item.RouteStep / lastItem.RouteStep;
                item.JumpsCountPercent = (double) item.JumpsCount / lastItem.JumpsCount;
                item.JumpedDistancePercent = (double) item.JumpedDistance / lastItem.JumpedDistance;
            }
        }
    }
}
