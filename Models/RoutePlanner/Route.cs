﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace EDUtils.Models
{
    public class Route : List<RouteStarSystem>, INotifyPropertyChanged, INotifyCollectionChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if(PropertyChanged!=null)
            {
                EDUtilsApp.Current.Dispatcher.Invoke((Action)delegate {
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                });
            }
        }
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public void OnCollectionChanged(NotifyCollectionChangedAction action=NotifyCollectionChangedAction.Reset)
        {
            this.OnPropertyChanged("CurrentIndex");
            this.OnPropertyChanged("CurrentStarSystem");
            this.OnPropertyChanged("NextStarSystem");
            if(CollectionChanged!=null)
            {
                EDUtilsApp.Current.Dispatcher.Invoke((Action)delegate {
                    NotifyCollectionChangedEventArgs ev = new NotifyCollectionChangedEventArgs(action);
                    CollectionChanged(this, ev);
                });
            }
        }
        
        protected int currentIndex = 0;

        public Route(IEnumerable<RouteStarSystem> collection)
            : base(collection)
        {
            this.Reset(collection);
        }

        public Route()
            : base()
        {
            this.Reset();
        }

        public int CurrentIndex {
            get { return this.currentIndex; }
        }
        public RouteStarSystem CurrentStarSystem {
            get { return this.Count>(currentIndex)? this[currentIndex] : null; }
        }
        public RouteStarSystem NextStarSystem {
            get { return this.Count>(currentIndex+1)? this[currentIndex+1] : null; }
        }
        public RouteStarSystem FirstStarSystem {
            get { return this.Count>0? this[0] : null; }
        }
        public RouteStarSystem LastStarSystem {
            get { return this.Count>0? this[this.Count-1] : null; }
        }

        public void Reset(IEnumerable<RouteStarSystem> items=null)
        {
            this.Clear();
            this.currentIndex = 0;
            if(items!=null && items.Count()>0)
            {
                this.AddRange(items);
            }
            this.OnCollectionChanged();
        }

        public bool SetCurrentStarSystem(string name)
        {
            bool found = false;
            if(!string.IsNullOrWhiteSpace(name))
            {
                int foundIndex = this.FindIndex(x=>x.name==name);
                if(foundIndex>=0)
                {
                    this.currentIndex = foundIndex;
                    found = true;
                }
            }
            this.OnPropertyChanged("CurrentIndex");
            this.OnPropertyChanged("CurrentStarSystem");
            this.OnPropertyChanged("NextStarSystem");
            //this.OnCollectionChanged();
            return found;
        }

        public bool SetCurrentStarSystem(StarSystem starSystem)
        {
            return this.SetCurrentStarSystem(starSystem?.name);
        }
    }
}
