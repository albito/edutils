﻿
using EDUtils.Utils;

namespace EDUtils.Models
{
    public class Coordinates : AbstractNotifyPropertyChanged
    {
        public float x = 0.0f;
        public float y = 0.0f;
        public float z = 0.0f;

        public float X {
            get { return this.x; }
        }
        public float Y {
            get { return this.y; }
        }
        public float Z {
            get { return this.z; }
        }
        public string XYZ {
            get { return string.Format("{0} {1} {2}", this.x, this.y, this.z); }
        }
        
        public Coordinates(float x=0.0f, float y=0.0f, float z=0.0f)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
}
