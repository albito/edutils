﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;
using EDUtils.Services;
using EDUtils.Services.EDEvents;
using EDUtils.Services.RoutePlanner;
using EDUtils.UI.Hotkey;
using EDUtils.UI.Overlay;
using static EDUtils.UI.Hotkey.Hotkey;

namespace EDUtils
{
    public partial class EDUtilsApp : Application
    {
        public static readonly string NAME = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToString();
        public static readonly string VERSION = new Regex(@"(.0)*$", RegexOptions.Compiled).Replace(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(), string.Empty);

        //TODO: Overlay view... https://github.com/lolp1/Overlay.NET/tree/master/src/Overlay.NET.Demo

        public EDUtilsApp()
        {
            this.Test();
        }

        public void Test()
        {

        }

        public RoutePlannerService routePlannerService = new RoutePlannerService();

        protected override void OnLoadCompleted(NavigationEventArgs e)
        {
            base.OnLoadCompleted(e);
            RegisterHotkeys();
            InitializeOverlayUI();
            EDEventMonitor.Start();
            ProcessMonitor.Start();
        }

        public void InitializeOverlayUI()
        {
            OverlayUI.Instace.AddRenderer(new RouteInfoOverlayRenderer(this.routePlannerService));
            OverlayUI.Instace.AddRenderer(new FuelLevelWarningOverlayRenderer());
        }

        private void RegisterHotkeys()
        {
            List<Hotkey> hotkeys = new List<Hotkey>();
            if(EDUtils.Properties.Settings.Default.HotkeyToggleOverlayUI>0)
            {
                hotkeys.Add(new Hotkey(10000, EDUtils.Properties.Settings.Default.HotkeyToggleOverlayUI, EDUtils.Properties.Settings.Default.HotkeyModifierToggleOverlayUI, OnToggleOverlayHotkeyEvent));
            }
            if(EDUtils.Properties.Settings.Default.HotkeyRoutePlannerCalculate>0)
            {
                hotkeys.Add(new Hotkey(10001, EDUtils.Properties.Settings.Default.HotkeyRoutePlannerCalculate, EDUtils.Properties.Settings.Default.HotkeyModifierRoutePlannerCalculate, OnRoutePlannerCalculateHotkeyEvent));
            }
            foreach(Hotkey hotkey in hotkeys)
            {
                HotkeyHelper.AddHotkey(hotkey);
            }
        }

        private void UnregisterHotkeys()
        {
            HotkeyHelper.RemoveHotkeys();
        }

        public void ResetHotkeys()
        {
            this.UnregisterHotkeys();
            this.RegisterHotkeys();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            UnregisterHotkeys();
        }

        private void OnToggleOverlayHotkeyEvent(object sender, HotkeyEventArgs e)
        {
            if(ProcessMonitor.GameProcess.IsRunning)
            {
                if (OverlayUI.Instace.IsEnabled)
                {
                    OverlayUI.Instace.Disable();
                }
                else
                {
                    OverlayUI.Instace.Enable();
                }
            }
        }

        private void OnRoutePlannerCalculateHotkeyEvent(object sender, HotkeyEventArgs e)
        {
            if(!this.routePlannerService.IsPlanning)
            {
                Task.Factory.StartNew(() => {
                        bool result = this.routePlannerService.TraceRouteAsync().Result;
                        if(!result)
                        {
                            MessageBox.Show(EDUtils.Resources.i18n.PlanningRouteFailedEnsureRouteValidStartEnd,
                                            EDUtils.Resources.i18n.Error,
                                            MessageBoxButton.OK,
                                            MessageBoxImage.Error);
                        }
                    });
            }
        }
    }
}
