﻿using EDUtils.Services;
using EDUtils.Services.Accounts;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace EDUtils.UI
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class Start : Page, INotifyPropertyChanged
    {
        public EDAccountsService AccountsService {
            get {
                return EDAccountsService.Instace;
            }
        }

        public Start()
        {
            InitializeComponent();
            DataContext = this;
            ProcessMonitor.LauncherProcess.PropertyChanged += OnPropertyChanged;
            ProcessMonitor.GameProcess.PropertyChanged += OnPropertyChanged;
            AccountsService.PropertyChanged += OnPropertyChanged;
            AccountsService.UpdateAccounts();
        }
        public bool CanLaunchSteam {
            get {
                return !ProcessMonitor.IsRunning
                    && ProcessMonitor.CanLaunchSteam;
            }
        }
        public bool CanLaunchStandalone {
            get {
                return !ProcessMonitor.IsRunning
                    && ProcessMonitor.CanLaunchStandalone;
            }
        }
        public string ProcessStatus {
            get {
                return ProcessMonitor.IsRunning?
                    string.Format(EDUtils.Resources.i18n.Running_0, (ProcessMonitor.GameProcess.IsRunning?
                                                                        EDUtils.Resources.i18n.Game
                                                                        : (ProcessMonitor.LauncherProcess.IsRunning?
                                                                            EDUtils.Resources.i18n.Launcher
                                                                            : EDUtils.Resources.i18n.None)))
                    : EDUtils.Resources.i18n.None;
            }
        }

        public bool CanSelectAccount {
            get {
                return !ProcessMonitor.IsRunning;
            }
        }

        private void LaunchStandaloneButtonClick(object sender, RoutedEventArgs ev)
        {
            if(CanLaunchStandalone)
            {
                ProcessMonitor.LaunchStandalone();
            }
        }

        private void LaunchSteamButtonClick(object sender, RoutedEventArgs ev)
        {
            if(CanLaunchSteam)
            {
                ProcessMonitor.LaunchSteam();
            }
        }

        private void SaveCurrentAccountButtonClick(object sender, RoutedEventArgs ev)
        {
            try
            {
                AccountsService.SaveCurrentAccount();
            }
            catch(Exception exception)
            {
                string message = string.Format(EDUtils.Resources.i18n.ErrorSaveCurrentAccount_0, exception.Message);
                MessageBox.Show(message,
                                EDUtils.Resources.i18n.Error,
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
            }
        }

        private void DeleteCurrentAccountButtonClick(object sender, RoutedEventArgs ev)
        {
            try
            {
                MessageBoxResult result = MessageBox.Show(EDUtils.Resources.i18n.ConfirmAccountDeletion,
                                EDUtils.Resources.i18n.ConfirmAccountDeletion,
                                MessageBoxButton.YesNoCancel,
                                MessageBoxImage.Warning);
                if(result==MessageBoxResult.OK
                    || result==MessageBoxResult.Yes)
                {
                    AccountsService.DeleteCurrentAccount();
                }
            }
            catch (Exception exception)
            {
                string message = string.Format(EDUtils.Resources.i18n.ErrorDeleteCurrentAccount_0, exception.Message);
                MessageBox.Show(message,
                                EDUtils.Resources.i18n.Error,
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
            }
        }

        private void SelectOtherAccountChanged(object sender, SelectionChangedEventArgs ev)
        {
            try
            {
                Object obj = ev.AddedItems.Count>0? ev.AddedItems[0] : null;
                if(obj is EDAccountConfiguration)
                {
                    EDAccountConfiguration account = (EDAccountConfiguration)obj;
                    AccountsService.ChangeCurrentAccount(account);
                }

            }
            catch(Exception exception)
            {
                string message = string.Format(EDUtils.Resources.i18n.ErrorChangeCurrentAccount_0, exception.Message);
                MessageBox.Show(message,
                                EDUtils.Resources.i18n.Error,
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                EDUtilsApp.Current.Dispatcher.Invoke((Action)delegate {
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                });
                if (propertyName == "IsRunning")
                {
                    this.OnPropertyChanged("ProcessStatus");
                    this.OnPropertyChanged("CanLaunchSteam");
                    this.OnPropertyChanged("CanLaunchStandalone");
                    this.OnPropertyChanged("CanSelectAccount");
                    this.OnPropertyChanged("CurrentAccount");
                }
            }
        }
        public void OnPropertyChanged(object source, PropertyChangedEventArgs ev)
        {
            this.OnPropertyChanged(ev.PropertyName);
        }
    }
}
