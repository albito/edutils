﻿using EDUtils.Services;
using EDUtils.Utils;
using Overlay.NET.Common;
using Overlay.NET.Directx;
using Process.NET;
using Process.NET.Windows;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Log = EDUtils.Utils.Log;

namespace EDUtils.UI.Overlay
{
    public class OverlayUI : DirectXOverlayPlugin
    {
        public const string CONFIG_OVERLAY_TIME = "OverlayTime";
        public const int CONFIG_OVERLAY_TIME_DEFAULT = 100;
        public static readonly int OVERLAY_TIME = AppConfig.GetAppValue(CONFIG_OVERLAY_TIME, CONFIG_OVERLAY_TIME_DEFAULT);

        public static readonly OverlayUI Instace = new OverlayUI();

        private readonly TickEngine tickEngine = new TickEngine();
        private Stopwatch chrono = new Stopwatch();
        private long lastTime;
        
        private static bool running = false;

        private static Task task;
        private static Task Task {
            get {
                if(task==null || task.IsCompleted)
                {
                    task = new Task(() => {
                        running = true;
                        while(running)
                        {
                            Instace.Update();
                            task.Wait(OVERLAY_TIME);
                        }
                    });
                }
                return task;
            }
        }

        private List<AbstractOverlayRenderer> renderers = new List<AbstractOverlayRenderer>();

        private OverlayUI() : base()
        {
            ProcessMonitor.GameProcess.ProcessStarted += OnGameStart;
            ProcessMonitor.GameProcess.ProcessExited += OnGameStop;
        }

        public void AddRenderer(AbstractOverlayRenderer renderer)
        {
            if(renderer!=null)
            {
                this.renderers.Add(renderer);
                this.RenderEvent += renderer.RenderEvent;
            }
        }

        public void RemoveRenderer(AbstractOverlayRenderer renderer)
        {
            this.renderers.Remove(renderer);
        }

        public override void Enable()
        {
            try
            {
                if(Properties.Settings.Default.OverlayUIEnabled)
                {
                    tickEngine.Interval = TimeSpan.FromMilliseconds(OVERLAY_TIME);
                    tickEngine.IsTicking = true;
                    base.Enable();
                    Task.Start();
                    chrono.Restart();
                }
            }
            catch(Exception exception)
            {
                Log.Warn("Exception on overlay enabling", exception, this.GetType(), "Enable()");
            }
        }

        public override void Disable()
        {
            try
            {
                tickEngine.IsTicking = false;
                this.ClearScreen();
                base.Disable();
                running = false;
                Task.Dispose();
            }
            catch(Exception exception)
            {
                Log.Warn("Exception on overlay disabling", exception, this.GetType(), "Disable()");
            }
        }

        private void OnGameStart(object src, EventArgs args)
        {
            try
            {
                if(src is System.Diagnostics.Process)
                {
                    System.Diagnostics.Process proc = (System.Diagnostics.Process)src;
                    ProcessSharp procS = new ProcessSharp(proc, Process.NET.Memory.MemoryType.Remote);
                    this.Initialize(procS.WindowFactory.MainWindow);
                    this.Enable();
                }
            }
            catch(Exception exception)
            {
                Log.Warn("Exception on overlay initializing", exception, this.GetType(), "OnGameStart(object src, EventArgs args)");
            }
        }

        private void OnGameStop(object src, EventArgs args)
        {
            if(src is System.Diagnostics.Process)
            {
                System.Diagnostics.Process proc = (System.Diagnostics.Process)src;
                running = false;
            }
        }

        public override void Initialize(IWindow targetWindow)
        {
            base.Initialize(targetWindow);
            OverlayWindow = new DirectXOverlayWindow(targetWindow.Handle, true);
            this.UpdateConfiguration();
            tickEngine.PreTick += OnPreTick;
            tickEngine.Tick += OnTick;
        }

        private DrawConfiguration drawConfiguration = new DrawConfiguration();

        public void UpdateConfiguration()
        {
            try
            {
                if(OverlayWindow?.Graphics!=null)
                {
                    this.drawConfiguration = new DrawConfiguration(OverlayWindow?.Graphics);
                }
                foreach(AbstractOverlayRenderer renderer in this.renderers)
                {
                    renderer.UpdateConfiguration();
                }
            }
            catch(Exception exception)
            {
                Log.Warn("Exception updating configuration", exception, this.GetType(), "UpdateConfiguration()");
            }
            if(!Properties.Settings.Default.OverlayUIEnabled)
            {
                this.Disable();
            }
        }

        public override void Update() => tickEngine.Pulse();

        private void OnTick(object sender, EventArgs e)
        {
            if(this.OverlayWindow?.Graphics!=null
                && this.OverlayWindow.IsVisible)
            {
                this.OverlayWindow.Update();
                this.Render();
            }
        }

        private void OnPreTick(object sender, EventArgs e)
        {
            bool targetWindowIsActivated = true;// TargetWindow.IsActivated;
            if(!targetWindowIsActivated && OverlayWindow.IsVisible)
            {
                ClearScreen();
                OverlayWindow.Hide();
            }
            else if(targetWindowIsActivated && !OverlayWindow.IsVisible)
            {
                OverlayWindow.Show();
            }
        }

        protected void Render()
        {
            OverlayWindow.Graphics.BeginScene();
            OverlayWindow.Graphics.ClearScene();
            if(this.RenderEvent!=null)
            {
                long totalTime = chrono.ElapsedMilliseconds;
                long time = chrono.ElapsedMilliseconds-lastTime;
                this.lastTime = totalTime;
                RenderEventArgs args = new RenderEventArgs(this.OverlayWindow, this.drawConfiguration, time, totalTime);
                this.RenderEvent.Invoke(this, args);
            }
            OverlayWindow.Graphics.EndScene();
        }

        private void ClearScreen()
        {
            if(OverlayWindow?.Graphics!=null)
            {
                OverlayWindow.Graphics.BeginScene();
                OverlayWindow.Graphics.ClearScene();
                OverlayWindow.Graphics.EndScene();
            }
        }

        public override void Dispose()
        {
            OverlayWindow.Dispose();
            base.Dispose();
        }

        public event EventHandler<RenderEventArgs> RenderEvent;

        public class RenderEventArgs : EventArgs
        {
            public readonly DirectXOverlayWindow window;
            public readonly Direct2DRenderer graphics;
            public readonly DrawConfiguration config;
            public long time;
            public long totalTime;

            public RenderEventArgs(DirectXOverlayWindow window, DrawConfiguration configuration, long time=0, long totalTime=0) : base()
            {
                this.window = window;
                this.graphics = window.Graphics;
                this.config = configuration;
                this.time = time;
                this.totalTime = totalTime;
            }
        }

        public class DrawConfiguration
        {
            public readonly int Font;
            public readonly int SmallFont;
            public readonly int BigFont;
            public readonly int Brush;
            public readonly int BorderBrush;
            public readonly int BackgroundBrush;

            public DrawConfiguration(Direct2DRenderer renderer=null)
            {
                if(renderer!=null)
                {
                    this.Font = renderer.CreateFont("Euro Caps", 14);
                    this.SmallFont = renderer.CreateFont("Euro Caps", 10);
                    this.BigFont = renderer.CreateFont("Euro Caps", 22, true);
                    this.Brush = renderer.CreateBrush(Properties.Settings.Default.OverlayUIColor);
                    this.BorderBrush = renderer.CreateBrush(Properties.Settings.Default.OverlayUIBorderColor);
                    this.BackgroundBrush = renderer.CreateBrush(Properties.Settings.Default.OverlayUIBackgroundColor);
                }
                else
                {
                    this.Font = 0;
                    this.SmallFont = 0;
                    this.BigFont = 0;
                    this.Brush = 0;
                    this.BorderBrush = 0;
                    this.BackgroundBrush = 0;
                }
            }
        }
    }
}
