﻿using EDUtils.Models.EDEvents;
using EDUtils.Services.EDEvents;
using Overlay.NET.Directx;
using static EDUtils.UI.Overlay.OverlayUI;

namespace EDUtils.UI.Overlay
{
    public class FuelLevelWarningOverlayRenderer : AbstractOverlayRenderer
    {
        public const int WIDTH = 170;
        public const int CRITICAL_WIDTH = 210;
        public const int HEIGHT = 45;

        private int x = 875;
        private int y = 50;

        public FuelLevelWarningOverlayRenderer() : base()
        {
            EDEventMonitor.OnGuiFocusStatusEvent += this.OnGuiFocusOrFuelStatusEvent;
            EDEventMonitor.OnFuelStatusEvent += this.OnGuiFocusOrFuelStatusEvent;
        }

        public void OnGuiFocusOrFuelStatusEvent(object sender, EDEventMonitor.StatusEventArgs eventArgs)
        {
            this.UpdateVisibility();
        }

        public override void UpdateVisibility()
        {
            this.visible = EDEventMonitor.status.Fuel.FuelMain <= Properties.Settings.Default.LowFuelWarningLevel
                            && (EDEventMonitor.status.GuiFocus == (int)EDStatusEvent.GUI_FOCUS.NONE
                                || EDEventMonitor.status.GuiFocus == (int)EDStatusEvent.GUI_FOCUS.GALAXY_MAP
                                || EDEventMonitor.status.GuiFocus == (int)EDStatusEvent.GUI_FOCUS.STATION_SERVICES);
        }

        public override void Draw(Direct2DRenderer graphics, DrawConfiguration config, DirectXOverlayWindow window, long time, long totalTime)
        {
            if((totalTime/1000)%2==0)
            {
                bool isCritical = EDEventMonitor.status.Fuel.FuelMain<=(Properties.Settings.Default.LowFuelWarningLevel/2);
                this.DrawBox(graphics, config, isCritical);
                this.DrawMessage(graphics, config, isCritical);
            }
        }

        private void DrawBox(Direct2DRenderer graphics, DrawConfiguration config, bool isCritical=false)
        {
            //graphics.DrawRectangle(x-(isCritical?((CRITICAL_WIDTH-WIDTH)/2):0), y, isCritical? CRITICAL_WIDTH : WIDTH, HEIGHT, 2, OverlayUI.BorderBrush);
            graphics.FillRectangle(x-(isCritical?((CRITICAL_WIDTH-WIDTH)/2):0), y, isCritical? CRITICAL_WIDTH : WIDTH, HEIGHT, config.BackgroundBrush);
        }

        private void DrawMessage(Direct2DRenderer graphics, DrawConfiguration config, bool isCritical=false)
        {
            string text = isCritical? Resources.i18n.CriticalFuelLevel : Resources.i18n.LowFuelLevel;
            graphics.DrawText(text, config.BigFont, config.Brush, x+5-(isCritical?((CRITICAL_WIDTH-WIDTH)/2):0), y+3);
            graphics.DrawText(Resources.i18n.FuelScoopableStars, config.Font, config.Brush, x+40, y+25);
        }

        public override void UpdateConfiguration()
        {
            this.x = Properties.Settings.Default.OverlayUIFuelLevelWarningX;
            this.y = Properties.Settings.Default.OverlayUIFuelLevelWarningY;
        }
    }
}
