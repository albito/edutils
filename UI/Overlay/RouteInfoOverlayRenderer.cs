﻿using EDUtils.Models;
using EDUtils.Models.EDEvents;
using EDUtils.Services.EDEvents;
using EDUtils.Services.RoutePlanner;
using Overlay.NET.Directx;
using System;
using static EDUtils.UI.Overlay.OverlayUI;

namespace EDUtils.UI.Overlay
{
    public class RouteInfoOverlayRenderer : AbstractOverlayRenderer
    {
        public static readonly int[,] LOGO_XY = new int[,] { { 18, 10 }, { 8, 20 }, { 12, 30 } };

        public const int WIDTH = 275;
        public const int HEIGHT = 40;

        private int x = 5;
        private int y = 5;
        private KeyCombination routeCalculationHotkey = new KeyCombination(Properties.Settings.Default.HotkeyRoutePlannerCalculate, Properties.Settings.Default.HotkeyModifierRoutePlannerCalculate);

        private RoutePlannerService routePlannerService;

        public RouteInfoOverlayRenderer(RoutePlannerService routePlannerService) : base()
        {
            this.routePlannerService = routePlannerService;
            EDEventMonitor.OnGuiFocusStatusEvent += this.OnGuiFocusStatusEvent;
        }

        public void OnGuiFocusStatusEvent(object sender, EDEventMonitor.StatusEventArgs eventArgs)
        {
            this.UpdateVisibility();
        }

        public override void UpdateVisibility()
        {
            this.visible = EDEventMonitor.status.GuiFocus == (int)EDStatusEvent.GUI_FOCUS.NONE;
        }

        public override void Draw(Direct2DRenderer graphics, DrawConfiguration config, DirectXOverlayWindow window, long time, long totalTime)
        {
            this.DrawBox(graphics, config);
            this.DrawLogo(graphics, config);
            this.DrawRouteTarget(graphics, config);
            if(this.routePlannerService.IsPlanning)
            {
                this.DrawRoutePlanning(graphics, config, totalTime);
            }
            else if(this.routePlannerService.HasRoute)
            {
                this.DrawRouteStatus(graphics, config);
            }
            else
            {
                this.DrawRouteHotkey(graphics, config);
            }
        }

        private void DrawBox(Direct2DRenderer graphics, DrawConfiguration config)
        {
            //graphics.DrawRectangle(x, y, WIDTH, HEIGHT, 2, config.BorderBrush);
            graphics.FillRectangle(x, y, WIDTH, HEIGHT, config.BackgroundBrush);
        }

        private void DrawLogo(Direct2DRenderer graphics, DrawConfiguration config)
        {
            graphics.DrawLine(x+LOGO_XY[0,0], y+LOGO_XY[0,1], x+LOGO_XY[1,0], y+LOGO_XY[1,1], 2, config.Brush);
            graphics.DrawLine(x+LOGO_XY[1,0], y+LOGO_XY[1,1], x+LOGO_XY[2,0], y+LOGO_XY[2,1], 2, config.Brush);
            graphics.DrawCircle(x+LOGO_XY[0,0], y+LOGO_XY[0,1], 1, 3, config.Brush);
            graphics.DrawCircle(x+LOGO_XY[1,0], y+LOGO_XY[1,1], 1, 3, config.Brush);
            graphics.DrawCircle(x+LOGO_XY[2,0], y+LOGO_XY[2,1], 1, 4, config.Brush);
        }

        private void DrawRouteTarget(Direct2DRenderer graphics, DrawConfiguration config)
        {
            string text = string.Format(Resources.i18n.TargetSystem_0, this.routePlannerService.TargetStarSystem?.Name);
            graphics.DrawText(text, config.SmallFont, config.Brush, x+25, y+3);
        }

        private void DrawRouteStatus(Direct2DRenderer graphics, DrawConfiguration config)
        {
            string text = string.Format(Resources.i18n.NextSystem_0, this.routePlannerService.Route.NextStarSystem?.Name);
            graphics.DrawText(text, config.SmallFont, config.Brush, x+25, y+13);

            RouteStarSystem currentStarSystem = this.routePlannerService.Route.CurrentStarSystem;
            RouteStarSystem firstStarSystem = this.routePlannerService.Route.FirstStarSystem;
            RouteStarSystem lastStarSystem = this.routePlannerService.Route.LastStarSystem;

            int jumps = currentStarSystem.JumpsCount;
            int step = currentStarSystem.RouteStep;
            int distance = (int)currentStarSystem.JumpedDistance;
            if(Properties.Settings.Default.OverlayUIShowRouteRemaining)
            {
                jumps = lastStarSystem.JumpsCount-currentStarSystem.JumpsCount;
                step = lastStarSystem.RouteStep-step;
                distance = (int)lastStarSystem.JumpedDistance-distance;
            }
            text = string.Format(Resources.i18n.Jumps_0_1_MapVisits_2_3_LY_4_5,
                                    jumps, lastStarSystem.JumpsCount,
                                    step, lastStarSystem.RouteStep,
                                    distance, (int)lastStarSystem.JumpedDistance);
            graphics.DrawText(text, config.SmallFont, config.Brush, x+25, y+23);


            int value = (int)(100*currentStarSystem.JumpedDistancePercent);
            graphics.DrawBarV(x+25, y+37, WIDTH-25, 3, value, 1, config.Brush, config.Brush);
        }

        private void DrawRoutePlanning(Direct2DRenderer graphics, DrawConfiguration config, long totalTime)
        {
            string text = Resources.i18n.PlanningRoute+("".PadRight((((int)totalTime) / 1000) % 4, '.'));
            graphics.DrawText(text, config.Font, config.Brush, x+25, y+20);
        }

        private void DrawRouteHotkey(Direct2DRenderer graphics, DrawConfiguration config)
        {
            string text = string.Format(Resources.i18n.Press_0_CalculateRoute, routeCalculationHotkey);
            graphics.DrawText(text, config.Font, config.Brush, x+25, y+20);
        }

        public override void UpdateConfiguration()
        {
            this.x = Properties.Settings.Default.OverlayUIRouteInfoX;
            this.y = Properties.Settings.Default.OverlayUIRouteInfoY;
        }
    }
}
