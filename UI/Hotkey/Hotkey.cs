﻿using EDUtils.Models;
using System;
using System.Windows.Input;

namespace EDUtils.UI.Hotkey
{
    public class Hotkey : KeyCombination
    {
        public readonly int id;
        public event EventHandler<HotkeyEventArgs> OnHotkey;

        public Hotkey(int id, Key key, ModifierKeys modifier=ModifierKeys.None, EventHandler<HotkeyEventArgs> onHotkey=null)
            : base(key, modifier)
        {
            this.id = id;
            if(onHotkey!=null)
            {
                this.OnHotkey += onHotkey;
            }
        }

        public Hotkey(int id, ushort key, ushort modifier=(ushort)ModifierKeys.None, EventHandler<HotkeyEventArgs> onHotkey=null)
            : base(key, modifier)
        {
            this.id = id;
            if(onHotkey!=null)
            {
                this.OnHotkey += onHotkey;
            }
        }

        protected internal void Trigger()
        {
            HotkeyEventArgs args = new HotkeyEventArgs(this);
            this.OnHotkey?.Invoke(this, args);
        }

        public override string ToString()
        {
            string keyString = string.Format("{1} ({0})", id, base.ToString());
            return keyString;
        }

        public static string ToString(Hotkey hotkey)
        {
            return hotkey!=null?
                string.Format("id={0}, key={1}, modifier={2}, VKKey={3}, VKModifier={4}", hotkey.id, hotkey.key, hotkey.modifier, hotkey.VKKey, hotkey.VKModifier)
                : "null";
        }

        public class HotkeyEventArgs : EventArgs{
            public readonly Hotkey hotkey;

            public HotkeyEventArgs(Hotkey hotkey) : base()
            {
                this.hotkey = hotkey;
            }
        }
    }
}
