﻿using EDUtils.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;

namespace EDUtils.UI.Hotkey
{
    //  Global hotkeys:
    //  https://stackoverflow.com/questions/2450373/set-global-hotkeys-using-c-sharp
    //  https://stackoverflow.com/questions/11377977/global-hotkeys-in-wpf-working-from-every-window
    public class HotkeyHelper
    {
        public const int WM_HOTKEY = 0x0312;

        private static readonly Dictionary<int, Hotkey> hotkeys = new Dictionary<int, Hotkey>();

        private static HwndSource source;

        private static readonly Window window = MainWindow();

        public static bool IsInitialized {
            get {
                return source!=null;// && !source.IsDisposed;
            }
        }
        
        [DllImport("User32.dll")]
        private static extern bool RegisterHotKey([In] IntPtr hWnd, [In] int id, [In] uint fsModifiers, [In] uint vk);
        [DllImport("User32.dll")]
        private static extern bool UnregisterHotKey([In] IntPtr hWnd, [In] int id);

        protected static Window MainWindow()
        {
            Window w = Application.Current.MainWindow;
            w.SourceInitialized += (object sender, EventArgs e) => {
                        Init();
                    };
            w.Closing += (object sender, CancelEventArgs e) => {
                        Destroy();
                    };
            return w;
        }

        protected static void Init()
        {
            try
            {
                Log.Info("Initializing hotkey handler", MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name);
                WindowInteropHelper helper = new WindowInteropHelper(window);
                source = HwndSource.FromHwnd(helper.Handle);
                source.AddHook(HwndHook);
                //Register hotkeys again just in case these had been registered before init
                List<Hotkey> values = hotkeys.Values.ToList();
                foreach(Hotkey hotkey in values)
                {
                    RegisterHotKey(hotkey);
                }
            }
            catch(Exception exception)
            {
                Log.Error("Exception initializing hotkey handler", exception, MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name);
            }
        }

        protected static void Destroy()
        {
            try
            {
                Log.Info("Disposing hotkey handler", MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name);
                List<Hotkey> values = hotkeys.Values.ToList();
                foreach(Hotkey hotkey in values)
                {
                    UnregisterHotkey(hotkey.id);
                }
                source.RemoveHook(HwndHook);
                source = null;
            }
            catch(Exception exception)
            {
                Log.Error("Exception disposing hotkey handler", exception, MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name);
            }
        }

        public static Hotkey GetHotkey(int id)
        {
            return hotkeys.ContainsKey(id)? hotkeys[id] : null;
        }

        public static bool IsHotkeyHandled(int id)
        {
            return hotkeys.ContainsKey(id);
        }

        private static bool RegisterHotKey(Hotkey hotkey)
        {
            bool result = false;
            try
            {
                if(IsInitialized)
                {
                    Log.Info("Registering hotkey " + Hotkey.ToString(hotkey), MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name);
                    WindowInteropHelper helper = new WindowInteropHelper(window);
                    result = RegisterHotKey(helper.Handle, hotkey.id, hotkey.VKModifier, hotkey.VKKey);
                    if(!result)
                    {
                        //We remove items if we cannot register them...
                        RemoveHotkey(hotkey.id);
                        Log.Warn("Cannot register hotkey " + Hotkey.ToString(hotkey), MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name);
                    }
                }
                else
                {
                    //We'll suppose it's allright
                    result = true;
                }
            }
            catch(Exception exception)
            {
                Log.Error("Exception registering hotkey "+Hotkey.ToString(hotkey), exception, MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name);
            }
            if(!result)
            {
                string message = string.Format(EDUtils.Resources.i18n.FailedToRegisterHotkey_0, hotkey?.ToString());
                MessageBox.Show(message,
                                EDUtils.Resources.i18n.Error,
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
            }
            return result;
        }

        private static bool UnregisterHotkey(int id)
        {
            bool result = false;
            try
            {
                Log.Info("Unregistering hotkey "+id, MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name);
                WindowInteropHelper helper = new WindowInteropHelper(window);
                result = UnregisterHotKey(helper.Handle, id);
            }
            catch(Exception exception)
            {
                Log.Error("Exception unregistering hotkey id="+id, exception, MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name);
                string message = string.Format(EDUtils.Resources.i18n.FailedToUnregisterHotkey_0, id);
                MessageBox.Show(message,
                                EDUtils.Resources.i18n.Error,
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
            }
            return result;
        }

        public static Hotkey AddHotkey(int id, Key key, ModifierKeys modifier)
        {
            Hotkey hotkey = null;
            AddHotkey(new Hotkey(id, key, modifier));
            if(hotkeys.ContainsKey(id))
            {
                hotkey = hotkeys[id];
            }
            return hotkey;
        }

        public static bool AddHotkey(Hotkey hotkey)
        {
            bool result = false;
            if(!hotkeys.ContainsKey(hotkey.id))
            {
                hotkeys.Add(hotkey.id, hotkey);
                result = RegisterHotKey(hotkey);
            }
            return result;
        }

        public static void RemoveHotkey(int id)
        {
            if(hotkeys.ContainsKey(id))
            {
                hotkeys.Remove(id);
            }
            UnregisterHotkey(id);
        }

        public static void RemoveHotkey(Hotkey hotkey)
        {
            RemoveHotkey(hotkey.id);
        }

        public static void RemoveHotkeys()
        {
            List<int> keys = new List<int>(hotkeys.Keys);
            foreach(int id in keys)
            {
                RemoveHotkey(id);
            }
        }

        private static void OnHotkeyPressed(int id)
        {
            if(hotkeys.ContainsKey(id))
            {
                OnHotkeyPressed(hotkeys[id]);
            }
        }

        private static void OnHotkeyPressed(Hotkey hotkey)
        {
            try
            {
                Log.Info("Handling hotkey event "+Hotkey.ToString(hotkey), MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name);
                hotkey.Trigger();
            }
            catch(Exception exception)
            {
                Log.Error("Exception handling hotkey "+Hotkey.ToString(hotkey), exception, MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name);
            }
        }

        private static IntPtr HwndHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if(msg==WM_HOTKEY)
            {
                int id = wParam.ToInt32();
                OnHotkeyPressed(id);
            }
            return IntPtr.Zero;
        }
    }
}
