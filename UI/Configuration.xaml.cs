﻿using EDUtils.Models;
using EDUtils.UI.Overlay;
using EDUtils.Utils;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace EDUtils.UI
{
    public partial class Configuration : Page
    {
        public Configuration()
        {
            InitializeComponent();
            Reset();
        }

        private void Save()
        {
            if(SteamExecutable.Tag!=null && SteamExecutable.Tag is string)
            {
                Properties.Settings.Default.SteamExePath = (string)SteamExecutable.Tag;
            }
            if(GameExecutable.Tag!=null && GameExecutable.Tag is string)
            {
                Properties.Settings.Default.GameExePath = (string)GameExecutable.Tag;
            }
            if(float.TryParse(ShipJumpRange.Text, out float shipJumpRange)
                && shipJumpRange>=10)
            {
                Properties.Settings.Default.ShipJumpRange = shipJumpRange;
            }
            if(ushort.TryParse(LowFuelWarningLevel.Text, out ushort lowFuelWarningLevel)
                && lowFuelWarningLevel>=0 && lowFuelWarningLevel<=100)
            {
                Properties.Settings.Default.LowFuelWarningLevel = lowFuelWarningLevel;
            }
            if(ushort.TryParse(RoutePlannerSpanshEfficiency.Text, out ushort spanshEfficiency)
                && spanshEfficiency>=0 && spanshEfficiency<=100)
            {
                Properties.Settings.Default.RoutePlannerSpanshEfficiency = spanshEfficiency;
            }
            if(RoutePlannerAutoCalculation.IsChecked.HasValue)
            {
                Properties.Settings.Default.RoutePlannerAutoCalculation = RoutePlannerAutoCalculation.IsChecked.Value;
            }
            bool updatedOverlayUI = false;
            if(OverlayUIEnabled.IsChecked.HasValue && OverlayUIEnabled.IsChecked.Value!=Properties.Settings.Default.OverlayUIEnabled)
            {
                Properties.Settings.Default.OverlayUIEnabled = OverlayUIEnabled.IsChecked.Value;
                updatedOverlayUI = true;
            }
            if(OverlayUIShowRouteRemaining.IsChecked.HasValue && OverlayUIShowRouteRemaining.IsChecked.Value!=Properties.Settings.Default.OverlayUIShowRouteRemaining)
            {
                Properties.Settings.Default.OverlayUIShowRouteRemaining = OverlayUIShowRouteRemaining.IsChecked.Value;
                updatedOverlayUI = true;
            }
            if(OverlayUIColor.SelectedColor.HasValue && ColorConverter.Convert(OverlayUIColor.SelectedColor.Value)!=Properties.Settings.Default.OverlayUIColor)
            {
                Properties.Settings.Default.OverlayUIColor = ColorConverter.Convert(OverlayUIColor.SelectedColor.Value);
                updatedOverlayUI = true;
            }
            if(OverlayUIBackgroundColor.SelectedColor.HasValue && ColorConverter.Convert(OverlayUIBackgroundColor.SelectedColor.Value)!=Properties.Settings.Default.OverlayUIBackgroundColor)
            {
                Properties.Settings.Default.OverlayUIBackgroundColor = ColorConverter.Convert(OverlayUIBackgroundColor.SelectedColor.Value);
                updatedOverlayUI = true;
            }
            if(OverlayUIBorderColor.SelectedColor.HasValue && ColorConverter.Convert(OverlayUIBorderColor.SelectedColor.Value)!=Properties.Settings.Default.OverlayUIBorderColor)
            {
                Properties.Settings.Default.OverlayUIBorderColor = ColorConverter.Convert(OverlayUIBorderColor.SelectedColor.Value);
                updatedOverlayUI = true;
            }
            if(ushort.TryParse(OverlayUIRouteInfoX.Text, out ushort overlayUIRouteInfoX)
                && overlayUIRouteInfoX!=Properties.Settings.Default.OverlayUIRouteInfoX)
            {
                Properties.Settings.Default.OverlayUIRouteInfoX = overlayUIRouteInfoX;
                updatedOverlayUI = true;
            }
            if(ushort.TryParse(OverlayUIRouteInfoY.Text, out ushort overlayUIRouteInfoY)
                && overlayUIRouteInfoY!=Properties.Settings.Default.OverlayUIRouteInfoY)
            {
                Properties.Settings.Default.OverlayUIRouteInfoY = overlayUIRouteInfoY;
                updatedOverlayUI = true;
            }
            if(ushort.TryParse(OverlayUIFuelLevelWarningX.Text, out ushort overlayUIFuelLevelWarningX)
                && overlayUIFuelLevelWarningX!=Properties.Settings.Default.OverlayUIFuelLevelWarningX)
            {
                Properties.Settings.Default.OverlayUIFuelLevelWarningX = overlayUIFuelLevelWarningX;
                updatedOverlayUI = true;
            }
            if(ushort.TryParse(OverlayUIFuelLevelWarningY.Text, out ushort overlayUIFuelLevelWarningY)
                && overlayUIFuelLevelWarningY!=Properties.Settings.Default.OverlayUIFuelLevelWarningY)
            {
                Properties.Settings.Default.OverlayUIFuelLevelWarningY = overlayUIFuelLevelWarningY;
                updatedOverlayUI = true;
            }
            bool updatedHotkeys = false;
            if(HotkeyToggleOverlayUI.Tag!=null && HotkeyToggleOverlayUI.Tag is KeyCombination)
            {
                KeyCombination keyConfig = (KeyCombination)HotkeyToggleOverlayUI.Tag;
                if((ushort)keyConfig.key!=Properties.Settings.Default.HotkeyToggleOverlayUI
                    || (ushort)keyConfig.modifier!=Properties.Settings.Default.HotkeyModifierToggleOverlayUI)
                {
                    Properties.Settings.Default.HotkeyToggleOverlayUI = (ushort)keyConfig.key;
                    Properties.Settings.Default.HotkeyModifierToggleOverlayUI = (ushort)keyConfig.modifier;
                    updatedHotkeys = true;
                }
            }
            if(HotkeyRoutePlannerCalculate.Tag!=null && HotkeyRoutePlannerCalculate.Tag is KeyCombination)
            {
                KeyCombination keyConfig = (KeyCombination)HotkeyRoutePlannerCalculate.Tag;
                if((ushort)keyConfig.key!=Properties.Settings.Default.HotkeyRoutePlannerCalculate
                    || (ushort)keyConfig.modifier!=Properties.Settings.Default.HotkeyModifierRoutePlannerCalculate)
                {
                    Properties.Settings.Default.HotkeyRoutePlannerCalculate = (ushort)keyConfig.key;
                    Properties.Settings.Default.HotkeyModifierRoutePlannerCalculate = (ushort)keyConfig.modifier;
                    updatedHotkeys = true;
                }
            }
            Properties.Settings.Default.Save();
            this.Reset();
            if(updatedHotkeys)
            {
                ((EDUtilsApp)EDUtilsApp.Current).ResetHotkeys();
                //TODO show a message if this doesn't work
            }
            if(updatedOverlayUI)
            {
                OverlayUI.Instace.UpdateConfiguration();
            }
        }

        private void Reset()
        {
            SteamExecutable.Content = Properties.Settings.Default.SteamExePath;
            GameExecutable.Content = Properties.Settings.Default.GameExePath;
            ShipJumpRange.Text = Properties.Settings.Default.ShipJumpRange.ToString();
            LowFuelWarningLevel.Text = Properties.Settings.Default.LowFuelWarningLevel.ToString();
            RoutePlannerSpanshEfficiency.Text = Properties.Settings.Default.RoutePlannerSpanshEfficiency.ToString();
            RoutePlannerAutoCalculation.IsChecked = Properties.Settings.Default.RoutePlannerAutoCalculation;
            OverlayUIEnabled.IsChecked = Properties.Settings.Default.OverlayUIEnabled;
            OverlayUIShowRouteRemaining.IsChecked = Properties.Settings.Default.OverlayUIShowRouteRemaining;
            OverlayUIColor.SelectedColor = ColorConverter.Convert(Properties.Settings.Default.OverlayUIColor);
            OverlayUIBackgroundColor.SelectedColor = ColorConverter.Convert(Properties.Settings.Default.OverlayUIBackgroundColor);
            OverlayUIBorderColor.SelectedColor = ColorConverter.Convert(Properties.Settings.Default.OverlayUIBorderColor);
            OverlayUIRouteInfoX.Text = Properties.Settings.Default.OverlayUIRouteInfoX.ToString();
            OverlayUIRouteInfoY.Text = Properties.Settings.Default.OverlayUIRouteInfoY.ToString();
            OverlayUIFuelLevelWarningX.Text = Properties.Settings.Default.OverlayUIFuelLevelWarningX.ToString();
            OverlayUIFuelLevelWarningY.Text = Properties.Settings.Default.OverlayUIFuelLevelWarningY.ToString();

            KeyCombination hotkeyToggleOverlayUI = new KeyCombination(Properties.Settings.Default.HotkeyToggleOverlayUI, Properties.Settings.Default.HotkeyModifierToggleOverlayUI);
            HotkeyToggleOverlayUI.Tag = hotkeyToggleOverlayUI;
            HotkeyToggleOverlayUI.Content = hotkeyToggleOverlayUI.ToString();
            KeyCombination hotkeyRoutePlannerCalculate = new KeyCombination(Properties.Settings.Default.HotkeyRoutePlannerCalculate, Properties.Settings.Default.HotkeyModifierRoutePlannerCalculate);
            HotkeyRoutePlannerCalculate.Tag = hotkeyRoutePlannerCalculate;
            HotkeyRoutePlannerCalculate.Content = hotkeyRoutePlannerCalculate.ToString();
        }

        private void SaveButtonClick(object sender, RoutedEventArgs e)
        {
            this.Save();
        }

        private void ResetButtonClick(object sender, RoutedEventArgs e)
        {
            this.Reset();
        }

        private void SteamExecutableButtonClick(object sender, RoutedEventArgs e)
        {
            string path = this.ShowFileDialog("Steam executable|Steam.exe", Properties.Settings.Default.SteamExePath);
            if(!string.IsNullOrWhiteSpace(path) && File.Exists(path))
            {
                SteamExecutable.Tag = path;
                SteamExecutable.Content = path;
            }
        }

        private void GameExecutableButtonClick(object sender, RoutedEventArgs e)
        {
            string path = this.ShowFileDialog("Game executable|EDLaunch.exe", Properties.Settings.Default.GameExePath);
            if(!string.IsNullOrWhiteSpace(path) && File.Exists(path))
            {
                GameExecutable.Tag = path;
                GameExecutable.Content = path;
            }
        }

        private string ShowFileDialog(string filter=null, string initialPath=null)
        {
            string path = null;
            System.Windows.Forms.OpenFileDialog fileDialog = new System.Windows.Forms.OpenFileDialog();
            if(!string.IsNullOrWhiteSpace(filter))
            {
                fileDialog.Filter = filter;
            }
            if(!string.IsNullOrWhiteSpace(initialPath))
            {
                initialPath = Path.GetDirectoryName(initialPath);
                fileDialog.InitialDirectory = initialPath;
            }
            System.Windows.Forms.DialogResult result = fileDialog.ShowDialog();
            switch(result)
            {
                case System.Windows.Forms.DialogResult.OK:
                    path = fileDialog.FileName;
                    break;
                case System.Windows.Forms.DialogResult.Cancel:
                default:
                    break;
            }
            return path;
        }

        private void KeyButtonClick(object sender, RoutedEventArgs e)
        {
            if(sender is Button)
            {
                Button button = (Button)sender;
                this.KeyButtonShowDialog(button, false);
            }
        }

        private void KeyButtonModifierClick(object sender, RoutedEventArgs e)
        {
            if(sender is Button)
            {
                Button button = (Button)sender;
                this.KeyButtonShowDialog(button, true);
            }
        }

        private void KeyButtonShowDialog(Button button, bool hasModifier=false)
        {
            KeyCombination keyConfig = button.Tag!=null && button.Tag is KeyCombination? (KeyCombination)button.Tag : null;
            ConfigurationKeyDialog keyDialog = new ConfigurationKeyDialog(keyConfig, hasModifier);
            if(keyDialog.ShowDialog()==true
                && !keyDialog.keyConfig.IsEmpty())
            {
                keyConfig = keyDialog.keyConfig;
                button.Tag = keyConfig;
                button.Content = keyConfig.ToString();
            }
        }

        private void RequiredTextBoxValidation(object sender, TextCompositionEventArgs e)
        {
            bool valid = e.Text.Trim().Length>0;
            e.Handled = valid;
        }

        private void NumberTextBoxValidation(object sender, TextCompositionEventArgs e)
        {
            bool valid = new Regex("[^0-9]+").IsMatch(e.Text);
            e.Handled = valid;
        }

        private void ShipJumpRangeTextBoxValidation(object sender, TextCompositionEventArgs e)
        {
            int value = 0;
            bool valid = e.Text.Trim().Length>0
                        && new Regex("[^0-9]+").IsMatch(e.Text)
                        && int.TryParse(e.Text, out value)
                        && value>=10;
            e.Handled = valid;
        }

        private void PercentTextBoxValidation(object sender, TextCompositionEventArgs e)
        {
            int value = 0;
            bool valid = e.Text.Trim().Length > 0
                        && new Regex("[^0-9]+").IsMatch(e.Text)
                        && int.TryParse(e.Text, out value)
                        && value>=0
                        && value<=100;
            e.Handled = valid;
        }
    }
}
