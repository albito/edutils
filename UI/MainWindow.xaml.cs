﻿using System;
using System.Windows;

namespace EDUtils.UI
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            LoadPosition();
            this.Closing += OnClosing;
        }

        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SavePositon();
        }

        public string WindowTitle {
            get {
                return EDUtilsApp.NAME+" "+EDUtilsApp.VERSION;
            }
        }

        private void LoadPosition()
        {
            double top = this.Top;
            double left = this.Left;
            double width = this.Width;
            double height = this.Height;
            if(Properties.Settings.Default.MainWindowTop >= 0)
            {
                top = Properties.Settings.Default.MainWindowTop;
            }
            if(Properties.Settings.Default.MainWindowLeft >= 0)
            {
                left = Properties.Settings.Default.MainWindowLeft;
            }
            if (Properties.Settings.Default.MainWindowWidth > 0
                && Properties.Settings.Default.MainWindowWidth >= this.MinWidth)
            {
                width = Properties.Settings.Default.MainWindowWidth;
            }
            if(Properties.Settings.Default.MainWindowHeight > 0
                && Properties.Settings.Default.MainWindowHeight >= this.MinHeight)
            {
                height = Properties.Settings.Default.MainWindowHeight;
            }
            height = Math.Min(height, SystemParameters.VirtualScreenHeight);
            width = Math.Min(width, SystemParameters.VirtualScreenWidth);
            if(top+(height/2) > SystemParameters.VirtualScreenHeight)
            {
                top = SystemParameters.VirtualScreenHeight - height;
            }
            if(left+(width/2) > SystemParameters.VirtualScreenWidth)
            {
                left = SystemParameters.VirtualScreenWidth - width;
            }
            top = Math.Max(top, 0);
            left = Math.Max(left, 0);

            this.Top = top;
            this.Left = left;
            this.Width = width;
            this.Height = height;
            if(Properties.Settings.Default.MainWindowState==(ushort)WindowState.Normal
                || Properties.Settings.Default.MainWindowState== (ushort)WindowState.Maximized)
            {
                this.WindowState = (WindowState)Properties.Settings.Default.MainWindowState;
            }
        }

        public void SavePositon()
        {
            if(this.WindowState!=WindowState.Minimized)
            {
                Properties.Settings.Default.MainWindowTop = this.Top;
                Properties.Settings.Default.MainWindowLeft = this.Left;
                Properties.Settings.Default.MainWindowWidth = this.Width;
                Properties.Settings.Default.MainWindowHeight= this.Height;
                Properties.Settings.Default.MainWindowState = (ushort)this.WindowState;
                Properties.Settings.Default.Save();
            }
        }

    }
}
