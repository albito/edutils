﻿
namespace EDUtils.Utils
{
    public class AppConfig
    {

        public static string GetAppValue(string key, string defaultValue=null)
        {
            string value = System.Configuration.ConfigurationManager.AppSettings[key];
            if(value==null)
            {
                value = defaultValue;
            }
            return value;
        }

        public static int GetAppValue(string key, int defaultValue)
        {
            string strValue = GetAppValue(key);
            int value = defaultValue;
            if(string.IsNullOrWhiteSpace(strValue)
                || int.TryParse(strValue, out value))
            {
                value = defaultValue;
            }
            return value;
        }
    }
}
