﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace EDUtils.Utils
{
    public class BooleanNegationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool negateValue = false;
            if(value!=null && value is bool)
            {
                negateValue = !((bool)value);    
                
            }
            return negateValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Convert(value, targetType, parameter, culture);
        }
    }

    public class BooleanToYesNoConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = "";
            string yes = Resources.i18n.Yes;
            string no = Resources.i18n.No;
            if(parameter!=null)
            {
                if(parameter is string)
                {
                    parameter = ((string)parameter).Split(';');    
                }
                if(parameter is string[] && ((string[])parameter).Length>=2)
                {
                    yes = ((string[])parameter)[0];
                    no = ((string[])parameter)[1];
                }
            }
            if(value is bool)
            {
                strValue = (bool)value==true? yes : no;
            }
            return strValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool boolValue = false;
            if(parameter!=null)
            {
                string yes = Resources.i18n.Yes;
                if(parameter is string)
                {
                    parameter = ((string)parameter).Split(';');    
                }
                if(parameter is string[] && ((string[])parameter).Length>=2)
                {
                    yes = ((string[])parameter)[0];
                }
                boolValue = value.ToString().Equals(yes, StringComparison.OrdinalIgnoreCase);
            }
            else
            {
			    switch(value.ToString().ToLower())
                {
                    case "yes":
                    case "y":
                    case "1":
                        boolValue = true;
                        break;
                    case "no":
                    case "n":
                    case "0":
                        boolValue = false;
                        break;
                    default:
                        break;
                }
            }
            return boolValue;
        }
    }
}
