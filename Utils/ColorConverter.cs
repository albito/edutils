﻿
namespace EDUtils.Utils
{
    public class ColorConverter
    {
        public static System.Windows.Media.Color Convert(System.Drawing.Color color)
        {
            return System.Windows.Media.Color.FromArgb(color.A, color.R, color.G, color.B);
        }
        public static System.Drawing.Color Convert(System.Windows.Media.Color color)
        {
            return System.Drawing.Color.FromArgb(color.A, color.R, color.G, color.B);
        }
    }
    
}
