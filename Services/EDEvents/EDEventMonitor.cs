﻿using EDUtils.Models.EDEvents;
using EDUtils.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDUtils.Services.EDEvents
{
    /// <summary>
    /// Monitors and handles the game events.
    /// </summary>
    public static class EDEventMonitor
    {
        /// <summary>
        /// The configuration parameter name for the check time.
        /// </summary>
        public const string CONFIG_EVENT_LOGS_CHECK_TIME = "EventLogsCheckTime";
        /// <summary>
        /// The configuration parameter default value for the check time.
        /// </summary>
        public const int CONFIG_EVENT_LOGS_CHECK_TIME_DEFAULT = 1000;
        /// <summary>
        /// The configured value for the check time.
        /// </summary>
        public static readonly int EVENT_LOGS_CHECK_TIME = AppConfig.GetAppValue(CONFIG_EVENT_LOGS_CHECK_TIME, CONFIG_EVENT_LOGS_CHECK_TIME_DEFAULT);

        /// <summary>
        /// THe jounral events reader.
        /// </summary>
        private static EDJournalEventsReader journalReader;

        /// <summary>
        /// The status events reader.
        /// </summary>
        private static EDStatusEventsReader statusReader;

        /// <summary>
        /// The current game status.
        /// </summary>
        public static EDStatusEvent status = new EDStatusEvent();

        /// <summary>
        /// Value which handles the thread loop.
        /// </summary>
        private static bool running = false;

        /// <summary>
        /// Thread loop which checks the game events.
        /// </summary>
        private static Task task = new Task(() => {
                running = true;
                statusReader = new EDStatusEventsReader();
                journalReader = new EDJournalEventsReader();
                while(running)
                {
                    CheckEvents();
                    task.Wait(EVENT_LOGS_CHECK_TIME);
                }
            });

        /// <summary>
        /// Starts the thread which checks the game events.
        /// </summary>
        public static void Start()
        {
            if(!running)
            {
                task.Start();
            }
        }

        /// <summary>
        /// Stops the thread which checks the game events.
        /// </summary>
        public static void Stop()
        {
            running = false;
        }

        /// <summary>
        /// Checks the latest game events.
        /// </summary>
        private static void CheckEvents()
        {
            CheckStatusEvent();
            CheckJournalEvent();
        }

        /// <summary>
        /// Checks the latest status game event.
        /// </summary>
        private static void CheckStatusEvent()
        {
            try
            {
                EDStatusEvent prevStatus = status;
                status = statusReader.ReadStatus();
                if(!status.Equals(prevStatus))
                {
                    StatusEventArgs eventArgs = new StatusEventArgs(status, prevStatus);
                    OnStatusEvent?.Invoke(status, eventArgs);
                    if(status.GuiFocus!=prevStatus.GuiFocus)
                    {
                        OnGuiFocusStatusEvent?.Invoke(status, eventArgs);
                    }
                    if(!status.Fuel.Equals(prevStatus.Fuel))
                    {
                        OnFuelStatusEvent?.Invoke(status, eventArgs);
                    }
                    if(status.Cargo!=prevStatus.Cargo)
                    {
                        OnCargoStatusEvent?.Invoke(status, eventArgs);
                    }
                    if(status.Pips!=prevStatus.Pips)
                    {
                        OnPipsStatusEvent?.Invoke(status, eventArgs);
                    }
                    if(status.Flags!=prevStatus.Flags)
                    {
                        OnFlagsStatusEvent?.Invoke(status, eventArgs);
                    }
                    if(status.FireGroup!=prevStatus.FireGroup)
                    {
                        OnFireGroupStatusEvent?.Invoke(status, eventArgs);
                    }
                    if(status.Longitude!=prevStatus.Longitude
                        || status.Latitude!=prevStatus.Latitude
                        || status.Altitude!=prevStatus.Altitude
                        || status.Heading!=prevStatus.Heading)
                    {
                        OnLocationStatusEvent?.Invoke(status, eventArgs);
                    }
                }
            }
            catch(Exception exception)
            {
                //TODO We should write a log line but not too often
            }
        }

        /// <summary>
        /// Checks the latest journal game events.
        /// </summary>
        private static void CheckJournalEvent()
        {
            try
            {
                IEnumerable<EDJournalEvent> events = journalReader.ReadEvents();
                if(events.Count()>0)
                {
                    foreach(EDJournalEvent jEvent in events)
                    {
                        ProccessJournalEvent(jEvent);
                    }
                }
            }
            catch(Exception exception)
            {
                //TODO We should write a log line but not too often
            }
        }

        /// <summary>
        /// Proccess a single journal event in order to handle the attached events.
        /// </summary>
        /// <param name="jEvent">The journal event.</param>
        private static void ProccessJournalEvent(EDJournalEvent jEvent)
        {
            try
            {
                JournalEventArgs eventArgs = new JournalEventArgs(jEvent);
                OnJournalEvent?.Invoke(jEvent, eventArgs);
                switch(jEvent.eventName)
                {
                    case EDJournalEvent.JOURNAL_EVENT.LOCATION:
                        OnLocationJournalEvent?.Invoke(jEvent, eventArgs);
                        break;
                    case EDJournalEvent.JOURNAL_EVENT.FSD_TARGET:
                        OnFSDTargetJournalEvent?.Invoke(jEvent, eventArgs);
                        break;
                    case EDJournalEvent.JOURNAL_EVENT.FSD_JUMP:
                        OnFSDJumpJournalEvent?.Invoke(jEvent, eventArgs);
                        break;
                    case EDJournalEvent.JOURNAL_EVENT.START_JUMP:
                        OnStartJumpJournalEvent?.Invoke(jEvent, eventArgs);
                        break;
                    case EDJournalEvent.JOURNAL_EVENT.JET_CONE_BOOST:
                        OnJetConeBoostJournalEvent?.Invoke(jEvent, eventArgs);
                        break;
                    default:
                        break;
                }
            }
            catch(Exception exception)
            {
                //TODO We should write a log line but not too often
            }
        }

        /// <summary>
        /// The status event args for event handling.
        /// </summary>
        public class StatusEventArgs : EventArgs {
                /// <summary>
                /// Current status.
                /// </summary>
                public readonly EDStatusEvent status;
                /// <summary>
                /// Previous status.
                /// </summary>
                public readonly EDStatusEvent previous;

                public StatusEventArgs(EDStatusEvent status, EDStatusEvent previous) : base() {
                        this.status = status;
                        this.previous = previous;
                    }
            };

        /// <summary>
        /// The journal event args for event handling.
        /// </summary>
        public class JournalEventArgs : EventArgs {
                /// <summary>
                /// Journal event.
                /// </summary>
                public readonly EDJournalEvent journal;

                public JournalEventArgs(EDJournalEvent journal) : base() {
                        this.journal = journal;
                    }
            };
        
        /// <summary>
        /// This is called on any status event.
        /// </summary>
        public static event EventHandler<StatusEventArgs> OnStatusEvent;
        /// <summary>
        /// This is called on flags changed.
        /// </summary>
        public static event EventHandler<StatusEventArgs> OnFlagsStatusEvent;
        /// <summary>
        /// This is called on gui focus changed.
        /// </summary>
        public static event EventHandler<StatusEventArgs> OnGuiFocusStatusEvent;
        /// <summary>
        /// This is called on fuel levels changed.
        /// </summary>
        public static event EventHandler<StatusEventArgs> OnFuelStatusEvent;
        /// <summary>
        /// This is called on cargo changed.
        /// </summary>
        public static event EventHandler<StatusEventArgs> OnCargoStatusEvent;
        /// <summary>
        /// This is called on pips changed.
        /// </summary>
        public static event EventHandler<StatusEventArgs> OnPipsStatusEvent;
        /// <summary>
        /// This is called on fire groups changed.
        /// </summary>
        public static event EventHandler<StatusEventArgs> OnFireGroupStatusEvent;
        /// <summary>
        /// This is called on location changed.
        /// </summary>
        public static event EventHandler<StatusEventArgs> OnLocationStatusEvent;

        /// <summary>
        /// This is called on any journal event.
        /// </summary>
        public static event EventHandler<JournalEventArgs> OnJournalEvent;
        /// <summary>
        /// This is called on location journal event.
        /// </summary>
        public static event EventHandler<JournalEventArgs> OnLocationJournalEvent;
        /// <summary>
        /// This is called on FSD target journal event.
        /// </summary>
        public static event EventHandler<JournalEventArgs> OnFSDTargetJournalEvent;
        /// <summary>
        /// This is called on FSD jump journal event.
        /// </summary>
        public static event EventHandler<JournalEventArgs> OnFSDJumpJournalEvent;
        /// <summary>
        /// This is called on FSD start jump journal event.
        /// </summary>
        public static event EventHandler<JournalEventArgs> OnStartJumpJournalEvent;
        /// <summary>
        /// This is called on jet cone boost journal event.
        /// </summary>
        public static event EventHandler<JournalEventArgs> OnJetConeBoostJournalEvent;
    }
}
