﻿using EDUtils.Models.EDEvents;
using EDUtils.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace EDUtils.Services.EDEvents
{
    /// <summary>
    /// Reads and handles the game events.
    /// </summary>
    /// <typeparam name="T">Event type</typeparam>
    abstract class EDLogsReader<T> where T:EDEvent
    {
        /// <summary>
        /// The default logs subfolder.
        /// </summary>
        public const string ED_LOGS_SUBFOLDER = "Saved Games\\Frontier Developments\\Elite Dangerous\\";
        /// <summary>
        /// The configuration parameter name for the logs path (for development purposes).
        /// </summary>
        public const string CONFIG_LOGS_PATH = "EDLogsPath";
        /// <summary>
        /// The configuration parameter default value for the logs path (for development purposes).
        /// </summary>
        public static readonly string CONFIG_LOGS_PATH_DEFAULT = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\" + ED_LOGS_SUBFOLDER;
        /// <summary>
        /// The configured value for the logs path (for development purposes).
        /// </summary>
        public static readonly string LOGS_PATH = AppConfig.GetAppValue(CONFIG_LOGS_PATH, CONFIG_LOGS_PATH_DEFAULT);
        /// <summary>
        /// The logs directory info.
        /// </summary>
        public static readonly DirectoryInfo LOGS_DIRECTORY = new DirectoryInfo(LOGS_PATH);

        /// <summary>
        /// The log file name.
        /// </summary>
        private string fileName = "";

        /// <summary>
        /// The log file name (read only access).
        /// </summary>
        public string FileName {
            get {
                return this.fileName;
            }
        }

        /// <summary>
        /// The log file.
        /// </summary>
        private StreamReader file;

        /// <summary>
        /// Initializes the reader and opens the file.
        /// </summary>
        protected EDLogsReader()
        {
            this.OpenFile();
        }

        /// <summary>
        /// Reads and returns the latest lines (from last read).
        /// </summary>
        /// <returns>Latest lines.</returns>
        public virtual List<string> GetLines()
        {
            List<string> lines = new List<string>();
            if(file!=null)
            {
                string line;
                while((line=file.ReadLine())!=null)  
                {
                    if(!string.IsNullOrWhiteSpace(line))
                    {
                        lines.Add(line);
                    }
                }
            }
            return lines;
        }

        /// <summary>
        /// Reads and returns the latest events (from last read).
        /// </summary>
        /// <returns>Latest events.</returns>
        public virtual List<T> ReadEvents()
        {
            List<string> lines = this.GetLines();
            List<T> events = lines.Select(x=>JsonConvert.DeserializeObject<T>(x)).ToList();
            return events;
        }

        /// <summary>
        /// Returns the log file info.
        /// </summary>
        /// <returns>The log file info.</returns>
        protected abstract FileInfo GetLogFile();

        /// <summary>
        /// Opens the log file for reading.
        /// </summary>
        protected void OpenFile()
        {
            try
            {
                FileInfo logFile = GetLogFile();
                Log.Info(string.Format("Opening events file {0}", logFile.FullName), this.GetType());
                FileStream fileStream = new FileStream(logFile.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                this.file = new StreamReader(fileStream, Encoding.Default);
                this.fileName = logFile.Name;
            }
            catch(Exception exception)
            {
                Log.Error("Exception opening file", exception, this.GetType());
            }
        }

        /// <summary>
        /// Resets the file position in order to start reading from the top.
        /// </summary>
        protected void ResetFile()
        {
            this.file.BaseStream.Position = 0;
            this.file.DiscardBufferedData();
        }

        /// <summary>
        /// Opens the file again.
        /// </summary>
        protected void ReloadFile()
        {
            this.OpenFile();
        }
    }
}
