﻿using EDUtils.Models.EDEvents;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EDUtils.Services.EDEvents
{
    /// <summary>
    /// Reads and handles the game journal events.
    /// </summary>
    class EDJournalEventsReader : EDLogsReader<EDJournalEvent>
    {
        /// <summary>
        /// Journal log file name pattern.
        /// </summary>
        public const string FILE_PATTERN = "journal.*.log";

        /// <summary>
        /// Initializes the journal events reader.
        /// </summary>
        public EDJournalEventsReader() : base()
        {

        }

        /// <summary>
        /// Reads the latest journal events from file.
        /// </summary>
        /// <returns>List of journal events.</returns>
        public override List<EDJournalEvent> ReadEvents()
        {
            List<EDJournalEvent> events = base.ReadEvents();
            if(events.Count()==0)
            {
                FileInfo logFile = this.GetLogFile();
                if(logFile!=null && !logFile.Name.Equals(this.FileName, StringComparison.OrdinalIgnoreCase))
                {
                    this.ReloadFile();
                }
            }
            return events;
        }

        /// <summary>
        /// Returns the journal log file info.
        /// </summary>
        /// <returns>Journal log file info.</returns>
        protected override FileInfo GetLogFile()
        {
            IEnumerable<FileInfo> files = LOGS_DIRECTORY.EnumerateFiles(FILE_PATTERN);
            files = files.OrderByDescending(x => x.CreationTime);
            FileInfo file = files.FirstOrDefault();
            return file;
        }
    }
}
