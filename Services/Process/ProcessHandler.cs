﻿using EDUtils.Utils;
using System;
using System.Diagnostics;

namespace EDUtils.Services
{
    class ProcessHandler : AbstractNotifyPropertyChanged
    {
        public readonly string ProcessName;
        private System.Diagnostics.Process process;

        public bool IsRunning {
            get {
                return this.process!=null && this.process.Responding;
            }
        }

        public ProcessHandler(string processName)
        {
            this.ProcessName = processName;
        }

        public EventHandler ProcessStarted;

        public EventHandler ProcessExited;

        public void UpdateProcess()
        {
            try
            {
                if(this.process==null)
                {
                    System.Diagnostics.Process[] foundProcesses = System.Diagnostics.Process.GetProcessesByName(this.ProcessName);
                    if(foundProcesses.Length>0)
                    {
                        this.process = foundProcesses[0];
                        Log.Info(string.Format("Detected {0} process", this.process.ProcessName), this.GetType(), "UpdateProcess()");
                        this.process.Exited += OnProcessExited;
                        if(this.ProcessStarted!=null)
                        {
                            this.ProcessStarted.Invoke(this.process, new EventArgs());
                        }
                        this.OnPropertyChanged("IsRunning");
                    }
                }
                else if(this.process.HasExited)
                {
                    this.process.Exited -= OnProcessExited;
                    Log.Info(string.Format("Detected {0} process exit", this.process.ProcessName), this.GetType(), "UpdateProcess()");
                    if(this.ProcessExited!=null)
                    {
                        this.ProcessExited.Invoke(this.process, new EventArgs());
                    }
                    this.process = null;
                    this.OnPropertyChanged("IsRunning");
                }
            }
            catch(Exception exception)
            {
                Log.Error(string.Format("Error updating proccess:", this.ProcessName), exception, this.GetType(), "UpdateProcess()");
            }
        }

        private void OnProcessExited(object sender, EventArgs ev)
        {
            this.UpdateProcess();
        }
    }
}
